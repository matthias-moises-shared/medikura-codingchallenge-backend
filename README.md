# CodingChallenge Backend

A boilerplate ExpressJS project will start and will reload automatically on every saved change.

- Clone project
- Create branch from `develop` in this format: `challenge-[yourname]`
- `yarn install`
- `yarn start`
- Do stuff
- Commit your changes in your branch
- Submit the repo as zip file
  
